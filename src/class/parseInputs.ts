export class ParseInputs{
  input: string[]

  constructor ( input: string[] ){
    this.input = input
  }


  parseInputs(){
    return {
      pathName: this.input[2],
      iterations: this.input[3],
      runs: this.input[4]
    }
  }

}

let input = new ParseInputs(process.argv)

console.log(input.parseInputs())